//------------------------------------------------------------------------------
// Lab1.cpp DT019G Object-Based Programming in C++
//------------------------------------------------------------------------------

#include "../include/Lab1.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include <iomanip>
#include <random>
#include "../include/Lab1.hpp"


// Returns good user input
size_t input(const char* input)
{
    size_t size;

    // Loop until user gives good input
    while(true)
    {
        std::cout << input << std::endl;
        std::cin >> size;

        // If user input is bad clear input; else escape loop
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else
            break;
    }

    return size;
}

void init()
{
    int *current, *end, *min, *max, *ptr;
    int number, sum = 0, y = 1;
    size_t i;

    size_t size = input("Input size of array: ");

    // Assign the user specified size to the dynamic array of int pointers
    int *v = new int[size];

    // Set pointers to the first element of the array
    ptr = &v[0];
    min = &v[0];
    max = &v[0];

    // Configuration to generate integers from -5000 to 5000
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(-5000, 5000);

    // Populate array with random generated integers
    for (i = 0; i < size; i++)
    {
        number = distribution(generator);
        v[i] = number;
    }

    std::cout << std::endl;

    // Loop through the entire array
    for (i = 0, current = v, end = &v[size]; current < end; i++, current++, ptr++)
    {
        // Add current entry to sum
        sum = sum + *ptr;

        // Store highest value in max
        if (*max < *current)
            max = current;

        // Store lowest value in min
        if (*min > *current)
            min = current;

        if (y == 1)
        {
            // Init while loop control
            int x = 1;

            // Prints the current element in the array, with a spacing of setw(10)
            std::cout << std::setw(10);
            std::cout << *ptr;
            
            // After 10 elements are printed, newline
            if ((i + 1) % 10 == 0)
                std::cout << std::endl;

            // After 200 elements are printed, ask user if he wants 200 more.
            if ((i + 1) % 200 == 0)
            {
                // Dont ask to print 200 more if the size = 200
                if (size == 200)
                    break;

                do
                {
                    size_t select = input("\n would you like to print 200 more? 1/0");
                    
                    if (select == 1)
                    {
                        // Clear Console
                        for (int l = 0; l < 40; l++)
                            std::cout << std::endl;
                        
                        // Prints the first line of the console
                        std::cout << "Input size of array:" << std::endl;
                        std::cout << size << std::endl << std::endl;

                        // Stop the while loop control
                        x = 0;

                        break;
                    }
                    
                    else if (select == 0)
                    {
                        // Stop the while loop control
                        x = 0;

                        // Stop the for loop control
                        y = 0;

                        break;
                    }

                } while (x == 1);
            }
        }
        
        // Else if user input is 0
        else if (y == 0)
            break;
    }

    // Output max, min and sum.
    std::cout << std::endl << std::endl;
    std::cout << "max:  " << *max << std::endl;
    std::cout << "min: " << *min << std::endl;
    std::cout << "sum: " << sum << "\n";

    // Destroy the array of pointers
    delete[]v;
}

int main()
{
    init();

    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
