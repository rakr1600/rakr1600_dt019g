//------------------------------------------------------------------------------
// Lab4.cpp DT019G Object-Based Programming in C++
//------------------------------------------------------------------------------

#include "../include/Lab4.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab4.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
