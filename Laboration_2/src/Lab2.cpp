//------------------------------------------------------------------------------
// Lab2.cpp DT019G Object-Based Programming in C++
//------------------------------------------------------------------------------

#include "../include/Lab2.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab2.hpp"


int main()
{
    Client client;

    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
