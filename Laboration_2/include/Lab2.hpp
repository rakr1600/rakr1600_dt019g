#include <vector>
#include <string>
#include <iomanip>
#include "etc.hpp"


class Address
{
private:
    std::string streetName;
    int zipCode;
    std::string city;

public:
    Address()
    {
        streetName = "";
        zipCode = 0;
        city = "";
    }

    void setStreetName(std::string input_street_name)
    {
        streetName = input_street_name;
    }

    void setZipCode(int input_zip_code)
    {
        zipCode = input_zip_code;
    }

    void setCity(std::string input_city)
    {
        city = input_city;
    }

    std::string getStreetName() const
    {
        return streetName;
    }

    int getZipCode() const
    {
        return zipCode;
    }

    std::string getCity() const
    {
        return city;
    }
};

class Name
{
private:
    std::string firstName;
    std::string lastName;

public:
    Name()
    {
        firstName = "";
        lastName = "";
    }
    
    void setFirstName(std::string first_name)
    {
        firstName = first_name;
    }

    void setLastName(std::string last_name)
    {
        lastName = last_name;
    }

    std::string getFirstName() const
    {
        return firstName;
    }

    std::string getLastName() const
    {
        return lastName;
    }
};

class Person : public Name, Address
{
private:
    Name name;
    Address address;

    std::string persNr;
    int skoNr;

    std::vector<Person> Pers_vec;

public:
    Person()
    {
        persNr = "";
        skoNr = 0;

        std::vector<Person> Pers_vec;
    }

    void setPersNr(std::string input_persNr)
    {
        persNr = input_persNr;
    }

    void setSkoNr(int input_skoNr)
    {
        skoNr = input_skoNr;
    }

    std::string getPersNr() const
    {
        return persNr;
    }

    int getSkoNr() const
    {
        return skoNr;
    }

    // Prints the entire vector
    void printList()
    {
        Etc etc;

        etc.clear_console();

        // Loops through the vector and prints all the corresponding elements of each entry
        for (size_t i = 0; i < Pers_vec.size(); i++)
        {
                std::cout << i + 1 << ' ' << std::setw(6);
                std::cout << ' ' << std::setw(6) << Pers_vec[i].name.getFirstName() << ' ' << std::setw(6);
                std::cout << ' ' << std::setw(6) << Pers_vec[i].name.getLastName() << ' ' << std::setw(6);

                std::cout << ' ' << std::setw(6) << Pers_vec[i].address.getStreetName() << ' ' << std::setw(6);
                std::cout << ' ' << std::setw(6) << Pers_vec[i].address.getZipCode() << ' ' << std::setw(6);
                std::cout << ' ' << std::setw(6) << Pers_vec[i].address.getCity() << ' ' << std::setw(6);

                std::cout << ' ' << std::setw(6) << Pers_vec[i].getPersNr() << ' ' << std::setw(6);
                std::cout << ' ' << std::setw(6) << Pers_vec[i].getSkoNr() << "\n\n";
        }

        etc.close_console();
    }

    // Pushes elements into the vector
    void setPerson()
    {
        this->setPerson("", "", "", 0, "", "", 0);
    }
    // Pushes elements into the vector
    void setPerson(std::string input_first_name, std::string input_last_name, std::string input_street_name, int input_zip_code, std::string input_city, std::string input_perNr, int input_skoNr)
    {
        Etc etc;
        Person person;
        
        // User input First Name
        if (input_first_name == "")
            person.name.setFirstName(etc.input("Input First Name\n"));
        else
            person.name.setFirstName(input_first_name);

        // User input Last Name
        if (input_last_name == "")
            person.name.setLastName(etc.input("\nInput Last Name\n"));
        else
            person.name.setLastName(input_last_name);

        // User input Street Name
        if (input_street_name == "")
            person.address.setStreetName(etc.input("\nInput Street Name\n"));
        else
            person.address.setStreetName(input_street_name);

        // User input Zip Code
        if (input_zip_code == 0)
            person.address.setZipCode(std::stoi(etc.input("\nInput Zip Code\n", 0)));
        else
            person.address.setZipCode(input_zip_code);

        // User input City
        if (input_city == "")
            person.address.setCity(etc.input("\nInput City\n"));
        else
            person.address.setCity(input_city);

        // User input Social Security Number
        if (input_perNr == "")
            person.setPersNr(etc.input("\nInput Social Security Number\n"));
        else
            person.setPersNr(input_perNr);


        // User input Shoe Size
        if (input_skoNr == 0)
            person.setSkoNr(std::stoi(etc.input("\nInput Shoe Size\n", 0)));
        else
            person.setSkoNr(input_skoNr);

        // Push back person to vector
        Pers_vec.push_back(person);
    }

    void navigation()
    {
        Etc etc;

        std::string selector = "";

        while(selector != "0")
        {
            etc.clear_console();

            std::cout << "________________________________________________" << "\n\n" << "                   Navigation" << "\n" << "________________________________________________" << "\n\n";

            selector = etc.input("[1] Add Data for a Person\n\n[2] Show Data for all Persons\n\n[0] Exit\n", 0);

            if (selector == "1")
                setPerson();

            if (selector == "2")
                printList();
        }
    }
};


class Client
{
private:

public:
    Client()
    {
        Person person;

        person.setPerson("John", "Doe", "Lane", 53211, "Milwaukee", "1234567890", 42);
        person.setPerson("Tim", "Johnson", "Court", 45501, "Springfield", "2345432553", 44);
        person.setPerson("Johnson", "Johnson", "Avenue", 98273, "Vernon", "3434534521", 43);

        person.navigation();
    }
};
