//------------------------------------------------------------------------------
// Lab3.cpp DT019G Object-Based Programming in C++
//------------------------------------------------------------------------------

#include "../include/Lab3.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab3.hpp"


int main()
{
    UserInterface userinterface;
    userinterface.run();

    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
