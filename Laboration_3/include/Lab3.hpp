#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "Etc.hpp"


class Address
{
private:
    std::string streetName;
    int zipCode;
    std::string city;

public:
    Address() = default;

    std::string getStreetName() const { return streetName; }
    void setStreetName(std::string streetName) { this->streetName = streetName; }

    int getZipCode() const { return zipCode; }
    void setZipCode(int zipCode) { this->zipCode = zipCode; }

    std::string getCity() const { return city; }
    void setCity(std::string city) { this->city = city; }
};

class Name
{
private:
    std::string firstName;
    std::string lastName;

public:
    Name() = default;
    
    std::string getFirstName() const { return firstName; }
    void setFirstName(std::string firstName) { this->firstName = firstName; }

    std::string getLastName() const { return lastName; }
    void setLastName(std::string lastName) { this->lastName = lastName; }
};

class Person : public Name, public Address
{
private:
    std::string persNr;
    int skoNr;

public:
    Person() = default;

    std::string getPersNr() const { return persNr; }
    void setPersNr(std::string persNr) { this->persNr = persNr; }
    
    int getSkoNr() const { return skoNr; }
    void setSkoNr(int skoNr) { this->skoNr = skoNr; }

    // Pushes elements into the vector
    void setPerson()
    {
        this->setPerson("", "", "", 0, "", "", 0);
    }
    // Pushes elements into the vector
    void setPerson(std::string firstName, std::string lastName, std::string streetName, int zipCode, std::string city, std::string persNr, int skoNr)
    {
        // User input First Name
        if (firstName == "")
            this->setFirstName(Etc::input("Input First Name\n"));
        else
            this->setFirstName(firstName);

        // User input Last Name
        if (lastName == "")
            this->setLastName(Etc::input("\nInput Last Name\n"));
        else
            this->setLastName(lastName);

        // User input Street Name
        if (streetName == "")
            this->setStreetName(Etc::input("\nInput Street Name\n"));
        else
            this->setStreetName(streetName);

        // User input Zip Code
        if (zipCode == 0)
            this->setZipCode(std::stoi(Etc::input("\nInput Zip Code\n", 0)));
        else
            this->setZipCode(zipCode);

        // User input City
        if (city == "")
            this->setCity(Etc::input("\nInput City\n"));
        else
            this->setCity(city);

        // User input Social Security Number
        if (persNr == "")
            this->setPersNr(Etc::input("\nInput Social Security Number\n"));
        else
            this->setPersNr(persNr);

        // User input Shoe Size
        if (skoNr == 0)
            this->setSkoNr(std::stoi(Etc::input("\nInput Shoe Size\n", 0)));
        else
            this->setSkoNr(skoNr);
    }

    bool operator== (const Person& rhs) const
    {
        return this == &rhs;
    }

    bool operator< (const Person& rhs) const
    {
        if (this->getLastName() == rhs.getLastName())
            return this->getFirstName() < rhs.getFirstName();
        return this->getLastName() < rhs.getLastName();
    }
};

std::ostream& operator<< (std::ostream &out, const Person &person)
{
    std::string result;

    result.append(person.getFirstName() + ',');
    result.append(person.getLastName() + ',');
    result.append(person.getStreetName() + ',');
    result.append(std::to_string(person.getZipCode()) + ',');
    result.append(person.getCity() + ',');
    result.append(person.getPersNr() + ',');
    result.append(std::to_string(person.getSkoNr()));

    out << result;
    return out;
}

std::istream& operator>> (std::istream &in, Person &person)
{
    std::string str;
    std::vector<std::string> arr;
    std::string delimiter = ",";

    in >> str;

    int pos = 0;
    while ((pos = str.find(delimiter)) != std::string::npos)
    {
        std::string value = str.substr(0, pos);
        arr.push_back(value);
        str.erase(0, pos + delimiter.length());
    }
    arr.push_back(str);

    if (arr.size() == 8)
    {
        person.setFirstName(arr[1]);
        person.setLastName(arr[2]);
        person.setStreetName(arr[3]);
        person.setZipCode(std::stoi(arr[4]));
        person.setCity(arr[5]);
        person.setPersNr(arr[6]);
        person.setSkoNr(std::stoi(arr[7]));
    }

    return in;
}

class PersonList
{
private:
    std::vector<Person> persons;
    std::string fileName;

public:
    PersonList()
    {
        this->fileName = "personList.csv";
    }

    std::string getFileName() { return this->fileName; }
    void setFileName(std::string fileName) { this->fileName = fileName; }

    void addPerson(Person person) { this->persons.push_back(person); }
    Person getPerson(int i) { return this->persons[i]; }
    int sizeOfPersons() { return this->persons.size(); }

    void sortName()
    {

    }
    void sortPersnr()
    {

    }
    void sortShoenr()
    {

    }

    void readFromFile()
    {
        std::ifstream inFile(this->getFileName());
        if (!inFile.is_open())
            std::cout << "unable to open file" << std::endl;

        int i = 0;
        std::string line;
        while (getline(inFile, line))
        {
            if (i == 0)
            {
                i++;
                continue;
            }

            Person newPerson;
            std::istringstream is(line);
            is >> newPerson;
            this->addPerson(newPerson);
        }
        inFile.close();
    }
    void writeToFile()
    {
        std::ofstream outFile;

        outFile.open (this->getFileName());
        outFile << "id,firstName,lastName,streetName,zipCode,city,persNr,skoNr" << std::endl;
        for (int i = 0; i < this->sizeOfPersons(); i++)
            outFile << i+1 << "," << this->getPerson(i) << std::endl;
        outFile.close();
    }
};

class UserInterface
{
private:
    PersonList personList;

    void addPerson()
    {
        this->addPerson("", "", "", 0, "", "", 0);
    }
    void addPerson(Person person)
    {
        this->personList.addPerson(person);
    }
    void addPerson(std::string firstName, std::string lastName, std::string streetName, int zipCode, std::string city, std::string persNr, int skoNr)
    {
        Person person;
        person.setPerson(firstName, lastName, streetName, zipCode, city, persNr, skoNr);
        this->personList.addPerson(person);
    }

    void printPersonList()
    {
        Etc::clear_console();

        std::cout << "id,firstName,lastName,streetName,zipCode,city,persNr,skoNr" << std::endl;

        for (int i = 0; i < personList.sizeOfPersons(); i++)
            std::cout << i+1 << "," << personList.getPerson(i) << std::endl;
    }

    void menu()
    {
        std::string selector = "";

        while(selector != "0")
        {
            Etc::clear_console();

            std::cout << "________________________________________________"
            << std::endl << std::endl
            << "                   Navigation"
            << std::endl
            << "________________________________________________"
            << std::endl << std::endl;

            std::string navInfo;
            navInfo.append("[1] Add Person \n\n");
            navInfo.append("[2] Show Persons \n\n");
            navInfo.append("[3] Read Data from file \n\n");
            navInfo.append("[4] Write Data to file \n\n");
            navInfo.append("[0] Exit \n\n");

            selector = Etc::input(navInfo, 0);

            if (selector == "1")
                this->addPerson();

            if (selector == "2")
                this->printPersonList();

            if (selector == "3")
                this->personList.readFromFile();

            if (selector == "4")
                this->personList.writeToFile();
            
            if (selector != "0" && selector != "1")
                Etc::close_console();
        }
    }

public:
    UserInterface() = default;

    void run()
    {
        this->addPerson("John", "Doe", "Lane", 53211, "Milwaukee", "1234567890", 42);
        this->addPerson("Tim", "Johnson", "Court", 45501, "Springfield", "2345432553", 44);
        this->addPerson("Johnson", "Johnson", "Avenue", 98273, "Vernon", "3434534521", 43);

        this->menu();
    }
};
