class Etc
{
private:

public:
    Etc() = default;

    /* Returns good user input; option = 0 filters non-numeric; option = 1 filters nothing (default);
    Example usage: etc.input("this is the text that gets printed to the console", 0);*/
    static std::string input(std::string input_text, int option = 1)
    {
        // option = 0 filters non-numeric
        if (option == 0)
        {
            std::string user_input_string = "";
            size_t user_input_size_t = 0;

            std::cout << input_text << '\n'; // Asks user for input

            // Loop until user gives good input
            while(true)
            {
                std::cin >> user_input_size_t; // Stores in user_input_size_t

                // If user input is bad clear input; else escape loop
                if (std::cin.fail())
                {
                    std::cin.clear();

                    // Filter non-numeric input
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                else
                {
                    break;
                }
            }
            
            user_input_string = std::to_string(user_input_size_t);

            return user_input_string;
        }
        
        // option = 1 filters nothing (default)
        else if (option = 1)
        {
            std::string user_input_string = "";

            std::cout << input_text << '\n'; // Asks user for input

            // Loop until user gives good input
            while(true)
            {
                std::cin >> user_input_string; // Stores in user_input_size_t

                // If user input is bad clear input; else escape loop
                if (std::cin.fail())
                {
                    std::cin.clear();
                }
                else
                {
                    break;
                }
            }

            return user_input_string;
        }
    }

    /* Cross Platform clear console */
    #if defined(__unix__) || defined(__linux__) || defined(__APPLE__) || defined(__MINGW32__) || defined(__MINGW64__)
    static void clear_console()
    {
        system("clear");
    }

    #elif defined(_WIN32) || defined(_WIN64)
    static void clear_console()
    {
        system("cls");
    }

    #else
    static void clear_console() { }

    #endif

    // Hang console on windows
    #if defined(_WIN32) || defined(_WIN64)
    static void hang_console()
    {
        std::cin.get();
    }

    #else
    static void hang_console() { }

    #endif

    /* Waits for User to press Enter */
    static void close_console()
    {
        std::cout << std::endl << std::endl << "Press enter to exit" << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.get();
    }
};
